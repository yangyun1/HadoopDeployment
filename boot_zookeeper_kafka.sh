#!/usr/bin/env bash

#使jdk-8u162-linux-x64.tar.gz用方法:
#1.在目标主机上配置ssh允许root用户登录
#2.在该脚本中修改参数
#3.在本机上执行脚本

SERVERS=("192.168.1.60" "192.168.1.63" "192.168.1.66")
SOFT_URL=http://192.168.1.179/soft

#配置SHH方法
ssh_copy_id() {
	echo "配置ssh免密登录..."
	ssh-keygen -t rsa
	for ip in ${SERVERS[@]}
	do
		ssh-copy-id root@${ip}
	done
}
#安装wget
install_wget() {
	echo "下载安装wget..."
	for ip in ${SERVERS[@]}
	do
		ssh root@$ip apt install -y wget
	done
}
#安装和配置JAVA
install_java() {
	echo "下载安装和配置JAVA..."
	for ip in ${SERVERS[@]}
	do
		scp install/java.sh root@$ip:/root
		ssh root@$ip "
		export SOFT_URL=$SOFT_URL
		sh /root/java.sh
		rm /root/java.sh
		"
	done
}
#安装和配置zookeeper
install_zookeeper() {
	echo "下载安装和配置zookeeper..."
	rm -f servers
	for ip in ${SERVERS[@]}
	do
		echo $ip >> servers
	done
	for i in "${!SERVERS[@]}"
	do
		scp install/zookeeper.sh root@${SERVERS[$i]}:/root
		scp servers root@${SERVERS[$i]}:/root
		ssh root@${SERVERS[$i]} "
		export SOFT_URL=$SOFT_URL
		export FILENAME='/root/servers'
		export myid=$i
		bash /root/zookeeper.sh
		rm /root/zookeeper.sh
		rm /root/servers
		"
	done
	rm servers
	echo "正在启动zookeeper集群..."
	for i in "${!SERVERS[@]}"
	do
		ssh root@${SERVERS[$i]} "
		source /etc/profile
		/usr/local/zookeeper/bin/zkServer.sh start
		echo '${SERVERS[$i]}节点启动结果:'
		/usr/local/zookeeper/bin/zkServer.sh status
		"
	done
}
#安装和配置kafka
install_kafka() {
	echo "下载安装和配置zookeeper..."
	rm -f servers
	for ip in ${SERVERS[@]}
	do
		echo ${ip} >> servers
	done
	for i in "${!SERVERS[@]}"
	do
		scp install/kafka.sh root@${SERVERS[$i]}:/root
		scp servers root@${SERVERS[$i]}:/root
		ssh root@${SERVERS[$i]} "
		export SOFT_URL=$SOFT_URL
		export FILENAME='/root/servers'
		export myid=$i
		bash /root/kafka.sh
		rm /root/kafka.sh
		rm /root/servers
		"
	done
	rm servers
	echo "正在启动kafka集群..."
	for i in "${!SERVERS[@]}"
	do
		ssh root@${SERVERS[$i]} "
		source /etc/profile
		cd /usr/local/kafka
		/usr/local/kafka/bin/kafka-server-start.sh -daemon config/server.properties 
		"
	done
}
ssh_copy_id
install_wget
install_java
install_zookeeper
install_kafka
