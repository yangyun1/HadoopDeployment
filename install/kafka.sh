#!/usr/bin/env bash

SERVERS=($(awk '{print $0}' $FILENAME))
#下载和解压安装包
wget $SOFT_URL/kafka_2.11-2.0.0.tgz
rm -rf /usr/local/kafka_2.11-2.0.0
tar -zxvf kafka_2.11-2.0.0.tgz -C /usr/local/
rm -rf kafka_2.11-2.0.0.tgz
rm -rf /usr/local/kafka
mv /usr/local/kafka_2.11-2.0.0 /usr/local/kafka
#配置环境变量
cat >> /etc/profile << EOF
export KAFKA_HOME=/usr/local/kafka
PATH=\$KAFKA_HOME/bin:\$PATH
EOF
#修改配置文件
sed -i 's/broker.id=0/broker.id='${myid}'/g' /usr/local/kafka/config/server.properties
sed -i 's?#listeners=PLAINTEXT://:9092?listeners=PLAINTEXT://'${SERVERS[${myid}]}':9092?g' /usr/local/kafka/config/server.properties
str="zookeeper.connect="
for ip in ${SERVERS[@]}
do
        str=${str}"$ip:2181,"
done
str=${str%*,}
sed -i 's/zookeeper.connect=localhost:2181/'${str}'/g' /usr/local/kafka/config/server.properties
