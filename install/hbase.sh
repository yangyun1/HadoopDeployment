#!/usr/bin/env bash

master=${MASTER}
slaves=($(awk '{print $0}' ${FILENAME}))

#下载和解压安装包
download_soft() {
echo "master:下载HBase安装包..."
wget ${SOFT_URL}/hbase-2.0.0-bin.tar.gz
rm -rf /usr/local/hbase-2.0.0
tar -zxvf hbase-2.0.0-bin.tar.gz -C /usr/local/
rm -rf hbase-2.0.0-bin.tar.gz
rm -rf /usr/local/hbase
mv /usr/local/hbase-2.0.0 /usr/local/hbase
}
#设置hadoop环境变量
set_env() {
echo "master:设置HBase环境变量..."
echo "export HBASE_HOME=/usr/local/hbase" >> /etc/profile
echo "export PATH=\$HBASE_HOME/bin:\$PATH" >> /etc/profile
echo "export HBASE_MANAGES_ZK=true" >> /etc/profile
}
#修改hbase-env.sh文件
set_hbase_env(){
echo "master:修改hbase-env.sh文件"
cat >> /usr/local/hbase/conf/hbase-env.sh << EOF
export JAVA_HOME=/usr/local/java
export HBASE_HOME=/usr/local/hbase
export PATH=\$PATH:/usr/local/hbase/bin
EOF
}
#修改hdfs-site.xml文件
set_hbase_site() {
echo "master:修改hbase-site.xml文件"
sed -i 's?<configuration>?<!--<configuration>-->?g' /usr/local/hbase/conf/hbase-site.xml
sed -i 's?</configuration>?<!--</configuration>-->?g' /usr/local/hbase/conf/hbase-site.xml
str=${master}
for ip in ${slaves[@]}
do
	str=${str}","${ip}
done
cat >> /usr/local/hbase/conf/hbase-site.xml << EOF
<configuration>
        <property>
                <name>hbase.rootdir</name>
                <value>hdfs://master:9000/hbase</value>
        </property>
        <property>
                <name>hbase.cluster.distributed</name>
                <value>true</value>
        </property>
        <property>
                <name>hbase.zookeeper.quorum</name>
                <value>${str}</value>
        </property>
        <property>
                <name>hbase.temp.dir</name>
                <value>/usr/local/hbase/tmp</value>
        </property>
        <property>
                <name>hbase.zookeeper.property.dataDir</name>
                <value>/usr/local/hbase/tmp/zookeeper</value>
        </property>
        <property>
                <name>hbase.master.info.port</name>
                <value>16010</value>
        </property>
</configuration>
EOF
}
#修改servers文件
set_regionservers(){
echo "master:修改regionservers文件"
rm /usr/local/hbase/conf/regionservers
echo "master" >> //usr/local/hbase/conf/regionservers
for i in "${!slaves[@]}"
do
    echo "slave""${i}" >> /usr/local/hbase/conf/regionservers
done
}

download_soft
set_env
set_hbase_env
set_hbase_site
set_regionservers


