#!/usr/bin/env bash

SERVERS=($(awk '{print $0}' $FILENAME))
#下载和解压安装包
wget $SOFT_URL/zookeeper-3.4.12.tar.gz
rm -rf /usr/local/zookeeper-3.4.12
tar -zxvf zookeeper-3.4.12.tar.gz -C /usr/local/
rm -rf zookeeper-3.4.12.tar.gz
rm -rf /usr/local/zookeeper
mv /usr/local/zookeeper-3.4.12 /usr/local/zookeeper
#配置环境变量
cat >> /etc/profile << EOF
export ZK_HOME=/usr/local/zookeeper
export PATH=\$ZK_HOME/bin:\$PATH
EOF
#修改配置文件
cp /usr/local/zookeeper/conf/zoo_sample.cfg /usr/local/zookeeper/conf/zoo.cfg
sed -i 's/dataDir=\/tmp\/zookeeper/dataDir=\/usr\/local\/zookeeper\/data/g' /usr/local/zookeeper/conf/zoo.cfg
for i in "${!SERVERS[@]}"
do
    echo "server.$i=${SERVERS[$i]}:2888:3888" >> /usr/local/zookeeper/conf/zoo.cfg
done
#创建数据文件夹
mkdir /usr/local/zookeeper/data
#在myid文件中添加本机的 server ID
echo "$myid" >> /usr/local/zookeeper/data/myid
