#!/usr/bin/env bash

#下载和解压安装包
wget ${SOFT_URL}/jdk-8u162-linux-x64.tar.gz
rm -rf /usr/local/jdk1.8.0_162
tar -zxvf jdk-8u162-linux-x64.tar.gz -C /usr/local/
rm -rf jdk-8u162-linux-x64.tar.gz
rm -rf /usr/local/java
mv /usr/local/jdk1.8.0_162 /usr/local/java
#配置环境变量
cat >> /etc/profile << EOF
export JAVA_HOME=/usr/local/java
export JRE_HOME=\$JAVA_HOME/jre
export CLASSPATH=.:\$JAVA_HOME/lib:\$JRE_HOME/lib:\$CLASSPATH
export PATH=\$JAVA_HOME/bin:\$JRE_HOME/bin:\$PATH
EOF
