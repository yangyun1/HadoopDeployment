# HadoopDeployment

#### 项目介绍
一键部署Kafka和Hadoop等大数据集群

#### 使用说明

1. 准备3台或者3台以上服务器用于搭建Kafka集群，准备3台或者3台以上用于搭建Hadoop集群
2. 服务器安装ubuntu操作系统
3. 设置ubuntu系统的root密码，下载安装ssh并设置允许root用户登陆。
4. 下载安装包，将安装包放到本机tomcat的webapps/ROOT/soft文件夹下(soft需要手动创建)，运行tomcat
5. 修改boot_zookeeper_kafka.sh文件中的服务器IP列表，在boot_zookeeper_kafka.sh所在目录下，用bash boot_zookeeper_kafka.sh启动安装。
6. 修改boot_hadoop_hbase_spark.sh文件中的服务器IP列表，在boot_hadoop_hbase_spark.sh所在目录下，用boot_hadoop_hbase_spark.sh启动安装。
7. 安装过程中会提示输入ssh的登陆密码，输入root用户的密码即可。